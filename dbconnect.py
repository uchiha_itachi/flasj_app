import pymongo


def connect(col_name):
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["mydatabase"]
    mycol = mydb[col_name]
    return mycol


def insert(collection, new_dic):
    collection.insert_one(new_dic)


def get_by_mail(collection, email):
    return collection.find_one({"email": email})


def add_student(collection, student):
    collection.insert_one(student)


def check_student_id(collection, student_id):
    for x in collection.find():
        if student_id == x["_id"]:
            return False
    return True


def sort_by(collection, byvalue):
    if byvalue == "StudentID":
        byvalue = "_id"
    list_student = list(collection.find())
    for i in range(0, len(list_student)-1):
        for j in range(i+1, len(list_student)):
            x = list_student[i][byvalue]
            y = list_student[j][byvalue]
            if (list_student[i][byvalue] > list_student[j][byvalue]):
                t = list_student[i]
                list_student[i] = list_student[j]
                list_student[j] = t
    return list_student


def delete_by(collection, deleteby, operator, value):
    list_student = collection.find()
    if deleteby == "StudentID":
        deleteby = "_id"
    if operator == "=":
        collection.delete_one({str(deleteby): value})
    elif operator == ">=":
        for x in list_student:
            if x[deleteby] >= value:
                collection.delete_one({str(deleteby): value})
    else:
        for x in list_student:
            if x[deleteby] <= value:
                collection.delete_one({str(deleteby): value})


# coll = connect("login")
# coll.drop()
# delete_by(coll, "StudentID", "=", "2")

# sort_by(coll, "math")
# for x in coll.find():
#     print(x["Name"])
