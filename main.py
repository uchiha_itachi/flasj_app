from flask import Flask, render_template, request, session, redirect, \
    url_for, send_file, make_response, flash
import dbconnect
from qrcode import make
import qrcode
import pyotp
import io
import werkzeug.security
app = Flask(__name__)
app.secret_key = "ajsdnas"


@app.route("/")
def home():
    email = request.cookies.get("email")
    username = None
    if email is not None:
        col = dbconnect.connect("login")
        username = dbconnect.get_by_mail(col, email)["username"]
    return render_template("home.html", email=email, username=username)


@app.route("/register", methods=["POST", "GET"])
def register():
    error = None
    if(request.method == "POST"):
        col = dbconnect.connect("login")
        username = request.form["username"]
        email = request.form["email"]
        password = request.form["password"]
        secret_key = pyotp.random_base32()
        user = dbconnect.get_by_mail(col, email)
        if(user is None):
            dic = {
                "username": username,
                "email": email,
                "password": werkzeug.security.generate_password_hash(password),
                "secret_key": secret_key
            }
            dbconnect.insert(col, dic)
            return render_template('qrcode.html', email=email)
        else:
            error = "Email has been used. Please try another email."
            return render_template("register.html", error=error)
    return render_template("register.html", error=error)


@app.route("/login", methods=["POST", "GET"])
def login():
    error = None
    if (request.method == "POST"):
        col = dbconnect.connect("login")
        email = request.form["email"]
        password = request.form["password"]
        optcode = request.form["optcode"]
        x = dbconnect.get_by_mail(col, email)
        hashpass = werkzeug.security.check_password_hash(x["password"],
                                                         password)
        if x is not None and hashpass:
            topt = pyotp.TOTP(x["secret_key"])
            if (topt.verify(str(optcode))) is True:
                resp = make_response(redirect(url_for("home")))
                resp.set_cookie("email", email, max_age=60*60)
                return resp
            else:
                error = "OPT code is not true."
                return render_template("login.html", error=error)
        else:
            error = "Email or password invaild"
            return render_template("login.html", error=error)
    return render_template("login.html", error=error)


@app.route("/qrcode/<email>")
def qrcode(email):
    col = dbconnect.connect("login")
    x = dbconnect.get_by_mail(col, email)
    topt = pyotp.TOTP(x["secret_key"])
    qr_code = make(topt.provisioning_uri(email))
    img = io.BytesIO()
    qr_code.save(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')


@app.route("/logout")
def logout():
    email = request.cookies.get("email")
    resp = make_response(redirect(url_for("home")))
    resp.set_cookie("email", email, max_age=0)
    return resp


@app.route("/studentlist", methods=["POST", "GET"])
def studentlist():
    student_col = dbconnect.connect("student")
    if request.method == "POST":
        byvalue = request.form["sortby"]
        student_list = dbconnect.sort_by(student_col, byvalue)
        return render_template("studentlist.html", students=student_list)
    student_list = student_col.find()
    return render_template("studentlist.html", students=student_list)


@app.route("/addstudent", methods=["POST", "GET"])
def addstudent():
    error = None
    if request.method == "POST":
        student_col = dbconnect.connect("student")
        student_id = request.form["studentID"]
        name = request.form["name"]
        date_of_birth = request.form["date"]
        math_score = float(request.form["math"])
        physic_score = float(request.form["physic"])
        english_score = float(request.form["english"])
        avg_score = (math_score + physic_score + english_score) / 3
        check = dbconnect.check_student_id(student_col, student_id)
        if check:
            student_dic = {
                "_id": student_id,
                "Name": name,
                "Date_of_birth": date_of_birth,
                "Math": math_score,
                "Physic": physic_score,
                "English": english_score,
                "Average": avg_score
            }
            dbconnect.add_student(student_col, student_dic)
            return redirect(url_for("studentlist"))
        else:
            error = "StudentID duplicate."
            return render_template("addstudent.html", error=error)

    return render_template("addstudent.html", error=error)


@app.route("/deletestudent", methods=["POST", "GET"])
def deletestudent():
    if request.method == "POST":
        delete_by = str(request.form["deleteby"])
        operator = request.form["operator"]
        value = request.form["value"]
        col = dbconnect.connect("student")
        dbconnect.delete_by(col, delete_by, operator, value)
    return redirect(url_for("studentlist"))


if __name__ == '__main__':
    app.run(debug=True)
